package de.sist.gitlab.integration;

import de.sist.gitlab.pipelinemonitor.gitlab.GraphQl;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.rnorth.ducttape.ratelimits.RateLimiterBuilder;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.utility.DockerImageName;

import java.net.URL;
import java.time.Duration;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static org.junit.Assume.assumeNotNull;

@SuppressWarnings({"rawtypes", "unchecked"})
public class IntegrationTest {
    private final String container = System.getenv("GITLAB_CONTAINER_TAG");
    private final String rootPassword = "initial_root_password";

    @Rule
    public GenericContainer gitlab = getContainer();

    private String projectPath;
    private String gitlabHost;
    private GitLabApi gitLabApi;

    private GenericContainer getContainer() {
        assumeNotNull(container);
        return new GenericContainer(DockerImageName.parse(this.container))
                .withExposedPorts(80)
                .waitingFor(
                        Wait.forHttp("/projects")
                        .withRateLimiter(RateLimiterBuilder
                                .newBuilder()
                                .withRate(1, TimeUnit.SECONDS)
                                .withConstantThroughput()
                                .build()
                        )
                )
                .withEnv("GITLAB_OMNIBUS_CONFIG", "gitlab_rails['initial_root_password'] = \"" + rootPassword + "\";")
                .withStartupTimeout(Duration.ofMinutes(50));
    }

    @Before
    public void setUp() throws Exception {
        gitlab.followOutput(new Slf4jLogConsumer(LoggerFactory.getLogger("GitlabContainer")));
        gitlabHost = new URL("http", gitlab.getHost(), gitlab.getMappedPort(80), "").toString();

        gitLabApi = GitLabApi.oauth2Login(gitlabHost, "root", rootPassword);

        projectPath = createProject();
    }

    private String createProject() throws GitLabApiException {
        String projectPath = "projectPath";
        gitLabApi.getProjectApi().createProject("", projectPath);
        return projectPath;
    }

    @Test
    public void testGraphQl() {
        GraphQl.makeCall(gitlabHost, gitLabApi.getAuthToken(), projectPath, Collections.singletonList(""));
    }
}
